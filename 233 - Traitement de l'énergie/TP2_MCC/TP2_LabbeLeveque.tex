\documentclass[a4paper,12pt]{article}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc}
\usepackage{soul}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{here}
\usepackage{ulem}
\usepackage[top=2cm,bottom=2cm,right=1.5cm,left=1.5cm]{geometry}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{appendix}



\begin{document}

\begin{center}
\textbf{\LARGE{Compte-Rendu de TP -- SAPHIRE}\\ \normalsize{\textit{\today}}}\\[0.4cm]
\LARGE{SAPH 253 C - Traitement de l'Énergie} \\[0.4cm]
\Large{Yann LABBÉ -- Olivier LÉVÊQUE} \\[0.5cm]
\LARGE{\uline{TP n$^\mathrm{o}$2 - Machines à courant continu}} \\[0.8cm]
\end{center}

\vspace{5\baselineskip}	%sauter 5 lignes
\tableofcontents
	%Il faut excuter le Latex deux fois d'affiler pour rafraichir le sommaire
\newpage

%-----------------------------------------------------------------
\section{Introduction}

\paragraph{}
Le but de ce TP est d'étudier la machine à courant continu.\\
Nous déterminerons, dans un premier temps, ses caractéristiques intrinsèques, tel que sa résistance interne, les pertes fer, les pertes mécaniques... Nous établirons, ensuite, les équations régissant le modèle du moteur à courant continu. Puis nous finirons sur une étude énergétique, en effectuant un bilan de puissance sur le système étudié pour déterminer le rendement des machines à courant continue par le biais de différentes méthodes.

\section{Mesure de la résistance d'induit}

\subsection{Objectif}

\paragraph{}
L'objectif est de déterminer l'ordre de grandeur de la résistance de l'induit.\\
Pour ce faire, nous allons mettre en oeuvre et comparer deux méthodes différentes : l'une portant sur l'utilisation d'un ohmmétre; l'autre utilisant la méthode volt-ampéremétrique.

\subsection{Manipulation}
\subsubsection{Utilisation d'un ohmmétre}

\paragraph{Principe de l'ohmmétre}
L'appareil fait circuler un courant de faible intensité $I$ (de l'ordre du $mA$ ou du $\mu A$) dans la résistance à mesurer et affiche le résultat $R=\frac{U}{I}$, où $U$ étant la tension aux bornes de la résistance (cf. Figure $1$).

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{ohmmetre.png}
\caption{\textit{Mesure à l'aide d'un ohmmétre}}
\end{minipage}
\end{figure}

\paragraph{Problème rencontré}
L'utilisation d'un ohmmétre semble être la solution adéquate pour mesurer la résistance interne d'un dipôle. Cependant ce n'est pas la cas pour l'induit qui comporte des éléments en contact qui faussent la mesure à l'ohmmètre.

\paragraph{}
Une machine à courant continu (MCC) se compose principalement de trois partie : un rotor (l'induit), un stator (l'inducteur) et d'un collecteur sur lequel frotte les balais.\\
Nous pouvons remarquer sur la Figure $2$ que le champ magnétique statique généré dans l'inducteur est dû à des bobines d'excitation. Cependant nous utilisons dans nos montages des MCC où ces bobines sont remplacer par des aimants permanents.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.4]{MCC.png}
\caption{\textit{Composition d'une machine à courant continu}}
\end{minipage}
\end{figure}

\paragraph{}
Ce sont ces "contacts" (les jonctions) entre le collecteur et les balais qui faussent les mesures à l'ohmmétre. Sur la Figure 3, on remarque que le jonction n'est pas parfaite et qu'une tension existe entre le collecteur et un balai.

\begin{figure}[H]
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=0.8]{collecteur_balais.png}
\caption{\textit{Schématisation de la jonction collecteur-balais}}
\end{minipage}
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=0.9]{schema_eq_elect.png}
\caption{\textit{Schéma électrique équivalent de la jonction collecteur-balais}}
\end{minipage}
\end{figure}

\paragraph{}
Le contact entre les balais et le collecteur se comporte un peu comme un semi-conducteur. Nous pouvons l'illustrer en traçant qualitativement l'allure de $u_{bc}$ en fonction de $I$, (cf. Figure 5). C’est en réalité l’air contenu entre les deux composants qui crée cette chute de tension. Si l'intensité augmente fortement l'air peut finir par se ioniser, changeant ainsi la conductivité du contact et donc la tension $u_{bc}$.\\
En utilisation normale, la MCC est traversé par quelques ampères qui est suffisant pour ioniser la jonction collecteur-balais. Il faut reproduire ces conditions pour mesurer la résistance de l'induit. Comme l'ohmmétre ne peut délivrer que quelques milliampères, il est donc obligatoire de changer de méthode.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=1]{caracteristique.png}
\caption{\textit{Caractéristique électriction de la jonction collecteur-balais}}
\end{minipage}
\end{figure}

\subsubsection{Méthode voltampèremétrique}

\paragraph{Principe de la méthode} La méthode voltampère-métrique est utilisée pour mesurer la résistance d'éléments devant être traversés par leur courant nominal. Elle s'appuie aussi sur la loi d'ohm : elle consiste à utiliser un ampèremètre (pour mesurer l'intensité du courant qui traverse l'élément considéré) et un voltmètre (pour la tension aux bornes de cet élément).\\
La résistance mesurée alors est : $R=\frac{U}{I}$.\\
Il existe cependant de configurations de branchement : l'une dite "montage courte dérivation" (cf. Figure 6); l'autre "montage longue dérivation" (cf. Figure 7).

\begin{figure}[H]
\begin{minipage}[b]{0.5\textwidth}
\centering
\includegraphics[scale=0.2]{3.png}
\caption{\textit{Montage courte dérivation}}
\end{minipage}
\begin{minipage}[b]{0.5\textwidth}
\centering
\includegraphics[scale=0.2]{2.png}
\caption{\textit{Montage longue dérivation}}
\end{minipage}
\end{figure}

\paragraph{}
La résistance que nous cherchons à mesurer est plutôt faible, donc de proche de celle de l'ampèremètre. Nous favoriserons alors le montage courte dérivation (cf. Figure 6) pour cette mesure, pour ne pas que le montage de l'ampèremètre en série affecte le relevé expérimental de la tension. Par ailleurs, le relevé expérimental de l'intensité n'en souffrira pas car la résistance interne du voltmètre est très grande comparée à celle que nous cherchons à quantifier.\\

\paragraph{}
Il est important de rappeler que le moteur ne doit pas tourner lorsqu'il est traversé par un courant lors de la mesure de $R_{induit}$. Sinon, la fem du moteur viendra s'ajouter à la tension générée par la résistance de l'induit et la relation $R_{induit}=\frac{U_{bornes moteur}}{I}$ deviendra fausse.\\
Pour éviter que le moteur tourne, nous utiliserons le montage décrit à la Figure $8$.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.8]{montage.png}
\caption{\textit{Schéma du montage}}
\end{minipage}
\end{figure}

\subsection{Relevés expérimentaux et analyses}

\paragraph{Mesure à l'ohmmètre}
Nous obtenons $R_{induit}=2,5 \Omega$ avec une mesure à l'ohmmètre.

\paragraph{Mesure par la méthode voltampéremètrique}
Après avoir effectué plusieurs relevés expérimentaux et tracé la caractéristique électrique de la MCC étudiée (cf. Figure $9$) autour des valeurs nominales de tension et de courant du système. Nous avons obtenu : $R_{induit}=0,958 \Omega$.

\begin{minipage}[b]{\textwidth}
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{carac_MMC.png}
\caption{\textit{Caractéristique électrique de la MCC étudiée}}
\end{figure}
\end{minipage}

\paragraph{}
Nous constatons, sans surprise, que les valeurs obtenues par ces deux méthodes est différentes. Nous garderons, comme expliqué précédemment, la valeur fournit par la méthode voltampéremètrique, soit $R_{induit}=0,958 \Omega$.

%-----------------------------------------------------------------
\section{Mesure du coefficient de conversion}

\subsection{Objectif}

\paragraph{}
Nous allons, à présent, chercher à déterminer le coefficient de proportionnalité entre la vitesse et la fem de la MCC. Cette relation qui est une relation de proportionnalité entre les deux grandeurs constitue la première équation du moteur à courant continu.

\subsection{Manipulation}

\paragraph{}
Nous avons à notre disposition deux MCC dont on a couplé leur arbre moteur ensemble. Nous alimenterons la première MCC, notée Moteur $A$, en tension continue réglable qui entrainera la deuxième MCC, notée Moteur $B$, qui quant à elle sera branchée à un voltmère (cf. Figure $10$). Nous pourrons relever la vitesse des arbres moteurs à l'aide d'une génératrice tachymétrique.

\begin{minipage}[b]{\textwidth}
\begin{figure}[H]
\centering
\includegraphics[scale=1]{schema1.png}
\caption{\textit{Schéma du montage pour mesurer le coefficient de conversion}}
\end{figure}
\end{minipage}

\paragraph{}
Pour obtenir une alimentation en tension continue pouvant débiter plusieurs ampère, nous utiliserons l'alimentation triphasée, disponible sur notre paillasse, que nous redresserons à l'aide d'un pont de diode. Puis nous lisserons le courant à l'aide d'une inductance.\\
Nous pouvons alors imposer une vitesse de rotation au Moteur $B$, en imposant une tension aux bornes du Moteur $A$ et relever la tension à ses bornes. En l'absence de courant circulant dans le Moteur $B$, la tension à ses bornes sera sa fem. Comme la vitesse est mesurer par la génératrice tachymétrique, nous pourrons finalement obtenir le coefficient de conversion liant le vitesse de rotation à la fem du moteur.

\paragraph{Remarque} On préfère donc travail sur Moteur $B$, plutôt que le $A$, car la relation entre la tension mesurée à ses bornes et la fem est directe : $U_{mesurée}=U_{fem}$. Contrairement au moteur $A$ qui n'est pas à vide, un courant de quelques ampères circule dans l'induit, ce qui provoque une chute de tension à ses bornes. Nous ne pourrions donc pas relever sa fem directement puisque : $U_{mesurée}=U_{fem}+R_{induit}I$.\\
Cependant, ayant calculé $R_{induit}$ précédemment il serait toujours possible de remonter à la fem du Moteur $A$.


\subsection{Relevés expérimentaux et analyses}

\paragraph{Donnée} \textit{Génératrice tachymètrique $K_{tachy}=6V/(1000tr.min^{-1})$, soit $K_{tachy}=0,0573$ $rad.s^{-1}$.}

\paragraph{}
Suite, à ce qu'il s'est précédemment dit, nous pouvons écrire :
\begin{itemize}
\item Pour le moteur $A$ : $\Omega=K.U_{fem}=K(U_{mesurée}-R_{induit}I)$
\item Pour le moteur $B$ : $\Omega=K.U_{fem}=K.U_{mesurée}$
\end{itemize}

\begin{figure}[H]
\begin{minipage}[b]{0.5\textwidth}
\centering
\includegraphics[scale=0.35]{conversionA.png}
\caption{\textit{Évolution de la tension aux bornes du moteur $A$ en fonction de sa vitesse de rotation}}
\end{minipage}
\begin{minipage}[b]{0.5\textwidth}
\centering
\includegraphics[scale=0.35]{conversionB.png}
\caption{\textit{Évolution de la fem (tension aux bornes) du moteur $B$ en fonction de sa vitesse de rotation}}
\end{minipage}
\end{figure}

\paragraph{}
D'après les relevés expérimentaux du moteur $B$, $K=0,18 V.s/rad$ ce qui se vérifie sur le moteur $B$ après correction de la chute de tension qu'engendre la résistance de l'induit (cf. Figure $13$).

\begin{minipage}[b]{\textwidth}
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{conversionA2.png}
\caption{\textit{Évolution de la fem du moteur $A$ en fonction de sa vitesse de rotation}}
\end{figure}
\end{minipage}

%-----------------------------------------------------------------
\section{Mesure des pertes à vide}

\subsection{Objectif}

\paragraph{}
Nous cherchons à présent à quantifier les pertes fer et les pertes mécaniques. Pour ensuite, dans la cinquième partie du TP, déterminer le rendement d'une machine à courant continu.

\subsection{Manipulation}

\paragraph{}
Il faut savoir qu'on pourra estimer les pertes fer+mécaniques mais qu'on sera dans l'incapacité de les séparer à partir des relevé expérimentaux.\\
En effet, les pertes fer et les pertes mécaniques évoluent quadratiquement en fonction de la vitesse de rotation du moteur (c.-à-d. évolue en $\Omega^2$). Pour les dissocier ces deux grandeurs, il faudrait "éteindre" le flux du champ magnétique généré par l'inducteur pour ne garder que les pertes mécaniques. Comme nos MMC sont constituées d'aimants permanents, il est impossible de les séparer. On considérera alors un ensemble de pertes étant les pertes fer + les pertes mécaniques.

\paragraph{Expérience :}
Nous avons gardé le montage précédent des deux MCCs couplées. Nous avons alimenté le moteur $A$ pour qu'il entraine en rotation le moteur $B$ à vide (cf. Figure $14$).

\begin{minipage}[b]{\textwidth}
\begin{figure}[H]
\centering
\includegraphics[scale=1]{schema2.png}
\caption{\textit{Schéma du disposif étudié}}
\end{figure}
\end{minipage}

\paragraph{}
Nous pouvons réaliser un bilan de puissance sur le dispositif étudié (cf. Figure $14$) :\\
\[ U_{moteur_A}I_{moteur_A}=R_{induit}I_{moteur_A}^2+2P_{fer+meca} \]\\
Nous constatons que le terme $P_{fer+meca}$ apparaît bien deux fois dans l'expression puisqu'il y a bien les pertes fer+méca du moteur $A$ mais aussi du moteur $B$, contrairement au perte par effet Joule qu'on ne compte qu'une seule fois, puisque le moteur $B$ n'est traversé par aucun courant : $P_{Joule}=R_{induit}I_{moteur_A}^2+R_{induit}I_{moteur_B}^2=R_{induit}I_{moteur_A}^2$ car $I_{moteur_B}=0$ puisque le moteur $B$ est à vide.

\subsection{Relevés expérimentaux et analyses}

\paragraph{}
En mesurant alors $U_{moteur_A}$ et $I_{moteur_A}$ nous pouvons déterminer $P_{fer+meca}$ pour différente vitesse de rotation, et tracer $P_{fer+meca}$ en fonction de $\Omega$ (la vitesse de rotation des MCC).

\begin{minipage}[b]{\textwidth}
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{pertes.png}
\caption{\textit{Évolue de $P_{fer+meca}$ en fonction de $\Omega$}}
\end{figure}
\end{minipage}

\paragraph{}
Nous retrouvons bien notre évolution quadratique des pertes fer+méca en fonction de la vitesse de rotation de la MCC, énoncé précédemment.


%-----------------------------------------------------------------
\section{Essais en charge}

\subsection{Objectif}

\paragraph{}
Pour finir notre étude sur les machines à courant continu, nous nous sommes intéressés à leur rendement et nous avons chercher à l'estimer à l'aide de deux méthodes différentes : la méthode directe et la méthode des pertes séparées.

\subsection{Manipulation}
On réalise le montage suivant :

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{image.png}
\caption{\textit{Schéma du dispositif}}
\end{figure}

En faisant varier la résistance de la charge, nous faisons également varier la vitesse de rotation. On prend soin de se placer au point de fonctionnement nominal du moteur et à re mettre la tension à sa valeur nominale pour chacune des mesures.

\paragraph{Méthode directe}
Cette première méthode consiste à supposer que le moteur $A$ et le moteur $B$ ont le même rendement, noté $\eta_{moteur}$, pour ensuite estimer le rendement de du système global, noté $\eta_{systeme}$ (moteur $A$ + moteur $B$) et poser : $\eta_{moteur}=\sqrt{\eta_{systeme}}$. Cependant, pour que le moteur $A$ et le moteur $B$ est le même rendement il faut émettre certaines hypothèses :
\begin{itemize}
\item chaque moteur doit avoir la même vitesse de rotation;
\item subir les mêmes pertes fer, mécaniques et par effet Joule.
\end{itemize}

\paragraph{}
Les deux moteurs ont bien la même vitesse de rotation puisqu'ils sont couplés ensemble, imposant alors les mêmes pertes fer+mécaniques puisqu'ils sont directement lié à la vitesse de rotation des moteurs.\\
Cependant, les pertes par effet Joule ne semble ne pas être identique sur les deux moteurs puisque les MCCs sont parcourues par des courants différents. Émettre l'hypothèse que les pertes par effet Joule sont identiques dans les deux moteurs peut altérer le résultat du rendement fournit par la méthode directe.

\paragraph{Expérience :}
On fixe la tension du moteur $A$ à la tension nominal pour ensuite mesurer toutes les grandeurs du circuit ($I_{moteur_A}$, $I_{moteur_B}$ et $U_{moteur_B}$).
Comme notre moteur $A$ ne présentait pas la tension nominale, mais plutôt sa vitesse de rotation nominale. Nous avons pu déduire une tension nominale de $40,6 V$ pour un $\Omega_{nominal} = 2000tr/min$.
Nous pouvons alors déduire le rendement du moteur soit :
\begin{itemize}
\item par la méthode directe avec :
\[ \eta_{moteur}=\sqrt{\eta_{systeme}}=\sqrt{\frac{U_{moteur_B}I_{moteur_B}}{U_{moteur_A}I_{moteur_A}}} \]
\item par la méthode des pertes séparées avec :
\[ \eta_{moteur}=\frac{U_{moteur_A}I_{moteur_A}-P_{fer+meca}-R_{induit}I_{moteur_A}^{2}}{U_{moteur_A}I_{moteur_A}} \]
\end{itemize}

\subsection{Relevés expérimentaux et analyses}

\paragraph{}
En appliquant les formules citées précédemment, nous pouvons établir le tableau suivant :

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{tableau.png}
\caption{\textit{Tableau de mesures}}
\end{minipage}
\end{figure}

On observe des différences de quelques pourcents dans le rendement entre les deux méthodes. Cela signifie que les deux moteurs  ne fonctionnent pas dans les mêmes conditions n'est pas complétement négligeable (hypothèse sur les pertes Joule à revoir, par exemple) et/ou que ces deux derniers ne soient pas exactement identique.\\
On peut remarquer aussi que le rendement augmente en fonction de la vitesse de rotation du moteur. Plus le moteur à une vitesse de rotation élevée plus son arbre a de l'inertie et donc plus faible est le couple à générer pour maintenir sa vitesse. Comme le couple est proportionnel à l'intensité, celle-ci diminue et les pertes Joule aussi. Le rendement en est donc augmenté.
 
%-----------------------------------------------------------------
\section{Conclusion}

\paragraph{}
Nous avons pu lors de ce TP étudier le fonctionnement d'une machine à courant continu. Nous avons vu comment déterminer ses paramèters intrinsèques. Ces mesures nous ont permis de déduire le rendement d'un moteur à courant continu. Cependant, pour cela nous avons dû faire des essais en charge. Dans l'industrie où les charges peuvent être trés importantes (par exemple pour un train), il n'est pas toujours possible de faire d'essais en charge. Il est toute fois possible de mesurer un rendement, sans pour autant réaliser l'expérience, à l'aide de ses propriétés intrinsèques données par le constructeur ($R_{induit}$, $P_{fer+meca} = f (\Omega)$.

\paragraph{}
En voici un exemple : Supposons que nous disposons sur notre palliasse d'une MCC dont les données constructeur indiquées les valeurs de $R_{induit}$, $K$ et  la courbe donnant $P_{fer+meca} $. Nous souhaitons connaître le rendement de cette machine pour un chargement de couple $C$ et de vitesse de rotation $\Omega$ connu.\\
\[ \eta=\frac{C\Omega}{UI} \]\\
avec :\\
\[ UI=RI^2+P_{fer+meca}+C\Omega \]\\
\[ U=RI+K\Omega \]\\
On en déduit,\\
\[ I=\frac{C\Omega+P_{fer+meca}}{K\Omega} \]\\
Ainsi le rendement peut entièrement s'exprimer en fonction de $C$ et $\Omega$ :
\[ \eta=\frac{C\Omega}{(R\frac{C\Omega+P_{fer+meca}}{K\Omega}+K\Omega)\frac{C\Omega+P_{fer+meca}}{K\Omega}} \]\\
Ce résultat montre que par le calcul on peut s'affranchir de l'expérience pour déterminer un rendement d'une machine à courant continu en un point de fonctionnement.



\end{document}