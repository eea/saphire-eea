\documentclass[a4paper,12pt]{article}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc}
\usepackage{soul}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{here}
\usepackage{ulem}
\usepackage[top=2cm,bottom=2cm,right=1.5cm,left=1.5cm]{geometry}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{appendix}



\begin{document}

\begin{center}
\textbf{\LARGE{Compte-Rendu de TP -- SAPHIRE}\\ \normalsize{\textit{\today}}}\\[0.4cm]
\LARGE{SAPH 253 C - Les Hacheurs} \\[0.4cm]
\Large{Yann LABBÉ -- Olivier LÉVÊQUE} \\[0.5cm]
\LARGE{\uline{TP n$^\mathrm{o}$1 - Hacheur série réversible en courant}} \\[0.8cm]
\end{center}

\vspace{5\baselineskip}	%sauter 5 lignes
\tableofcontents
	%Il faut excuter le Latex deux fois d'affiler pour rafraichir le sommaire
\newpage

%-----------------------------------------------------------------
\section{Introduction}

\subsection{Présentation du TP}

\paragraph{}
On se propose d'étudier le fonctionnement d'un hacheur abaisseur de tension (cf. Figure $1$). Cette structure élémentaire à transfert direct de puissance est couramment exploitée dans les systèmes de conversion de faibles et fortes puissances.\\
Elle offre, d'autre part, un parfait support expérimental pour appréhender les principes fondamentaux de l'électronique de puissance.\\
Nous allons donc étudier le fonctionnement d'un hacheur abaisseur de tension et mettre en évidence les points suivant :
\begin{itemize}
\item étude de la loi de commande,
\item étude de la cellule de filtrage en sortie,
\item étude de la réversibilité en courant,
\item étude de la caractéristique statique de sortie en fonction du mode de conduction.
\end{itemize}

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{1.png}
\caption{\textit{Structure du hacheur série étudié}}
\end{minipage}
\end{figure}

\paragraph{}
Le module de puissances, disponible dans la salle de TP, est constitué de transistors de type IGBT (Isulated Grid Bipolar Transistor) commandables à l'amorçage et au blocage et de diodes montées en "anti-parallèle" sur ces derniers. L'ensemble se présente comme une brique dotée de trois entrées et deux sorties (cf. Figure $2$).\\
L'association d'interrupteurs définit une cellule de commutation réversible en courant. Toutefois, afin d'assurer l'étude complète de cette cellule, les interrupteurs $S_1$ et $S_2$ permettent respectivement d'inhiber les interrupteur de puissance $T_1$ et $T_2$.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{2.png}
\caption{\textit{Cellule de commutation}}
\end{minipage}
\end{figure}

\paragraph{}
Les valeurs suivantes de composants électroniques, visible sur la figure $1$, nous sont proposées :\\
\begin{center}
\(L=500\mu H\), \(C=470\mu F\), \(R=[0,50]\Omega\), \(F_{dec}=[5,30]kHz\) et \(\alpha=[0.1,0.9]\)
\end{center}

\subsection{Travail préparatoire}
\paragraph{}
Fixons $\alpha=50\%$ et $R=10\Omega$.
Nous allons chercher à :
\begin{itemize}
\item Tracer de manière qualitative les formes d'ondes de $V_{cell}$, $i_L$ et $i_e$.
\item Calculer la valeur moyenne du courant de sortie, notée $I_s$.
\item Calculer l'ondulation de courant dans l'inductance, notée $\Delta i_L$.
\item Pour la configuration $1$ quadrant ($S_2$ ouvert), déterminer le mode de conduction (continue, discontinue).
\item Déterminer l'ondulation résiduelle de la tension aux bornes de la résistance, notée $\Delta u_s$.
\end{itemize}

\subsubsection{Tracé qualitatif des formes d'ondes de $V_{cell}$, $i_L$ et $i_e$}
\paragraph{}
Le tracé qualitatif des formes d'ondes de $V_{cell}$, $i_L$ et $i_e$ se détermine par la superposition des deux modes de fonctionnement qu'offre la structure du hacheur série étudié.\\
Étudions le fonctionnement $1$ quadrant $S_1$ fermé et $S_2$ ouvert (cf. Figure $3$), puis le fonctionnement $1$ quadrant $S_1$ ouvert et $S_2$ fermé (cf. Figure $4$).

\begin{figure}[H]
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=1]{img3.png}
\caption{\textit{Structure $1$ quadrant $S_1$ fermé et $S_2$ ouvert}}
\end{minipage}
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=1]{img4.png}
\caption{\textit{Structure $1$ quadrant $S_1$ ouvert et $S_2$ fermé}}
\end{minipage}
\end{figure}

\paragraph{}
Avec l'expression du courant traversant une bobine, on en déduit l'allure de $i_L$ dans les $4$ cas possibles :
\[ u_L=L\frac{di_L}{dt} \]
d'où \[ i_L=\frac{u_L}{L}t+I_0 \]
avec : $u_L= \frac{V_e-V_s}{L}$ si $t \in [0,\alpha T]$ et $u_L= -\frac{V_s}{L}$ si $t \in [\alpha T,T]$

\begin{figure}[H]
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=1]{img1.png}
\caption{\textit{Tracé de $V_{cell}(t)$, $i_L(t)$ pour $S_1$ fermé uniquement}}
\end{minipage}
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=1]{img2.png}
\caption{\textit{Tracé de $V_{cell}(t)$, $i_L(t)$ pour $S_2$ fermé uniquement}}
\end{minipage}
\end{figure}

\paragraph{Remarque :}
$i_e=i_L$ lorsque $t\in [0,\alpha T]$ sinon $i_e=0$.

\paragraph{}
Nous en déduisons alors, par superposition des deux cas étudiés, les formes d'ondes de $V_{cell}$, $i_L$ et $i_e$ pour une fonctionnement $2$ quadrants ($S_1$ et $S_2$ fermés).

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=1]{img.png}
\caption{\textit{Tracé de $V_{cell}(t)$, $i_L(t)$ pour $S_1$ et $S_2$ ouverts}}
\end{minipage}
\end{figure}

\paragraph{Remarque :}
de nouveau on obtient $i_e=i_L$ lorsque $t\in [0,\alpha T]$ sinon $i_e=0$.

\subsubsection{Valeur moyenne du courant de sortie}
\paragraph{}
La valeur moyenne du courant de sortie s'obtient de la manière suivante :
\[ I_s=<i_S>=<i_L>-<i_C>=<i_L>-C<\frac{dv_S}{dt}> \]
Comme la tension $v_s$ aux bornes du condensateur est périodique, on en déduit $<\frac{dv_S}{dt}>=0$. On obtient donc :
\[ I_s=<i_L>=\frac{I_{max}+I_{min}}{2} \]
\[ I_s=\frac{V_e-V_S}{2L}\alpha T +I_0=\frac{(1-\alpha)V_e}{2L}\alpha T +I_0 \]

\subsubsection{Ondulation de courant dans l'inductance}
\paragraph{}
L'ondulation du courant dans l'inductance a pour expression :
\[ \Delta I_L=I_{max}-I_{min}=\frac{V_e-V_S}{L}\alpha T=\frac{(1-\alpha)V_e}{2L}\alpha T \]

\subsubsection{Mode de conduction pour une structure $1$ quadrant}
\paragraph{}
Pour une configuration 1 quadrant ($S_1$ fermé et $S_2$ ouvert), nous pouvons déterminer à quelle condition nous avons un mode de conduction fermé ou ouvert.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=1]{img3.png}
\caption{\textit{Structure du hacheur avec $S_1$ fermé et $S_2$ ouvert}}
\end{minipage}
\end{figure}

\begin{minipage}[b]{0.45\textwidth}
\centering
\textbf{Mode continu}\\
Nous avons un mode de conduction continue si la condition suivante est respectée :
\[ \frac{\Delta I_L}{2}<I_L \]
On en déduit, à l'aide des expressions précédemment établies :
\[ I_0>0 \]
\end{minipage}
\begin{minipage}[b]{0.45\textwidth}
\centering
\textbf{Mode discontinu}\\
Nous avons un mode de conduction discontinue si la condition suivante est respectée :
\[ \frac{\Delta I_L}{2}>I_L \]
On en déduit, à l'aide des expressions précédemment établies :
\[ I_0<0 \]
\end{minipage}

\subsubsection{Ondulation résiduelle de la tension aux bornes de la résistance}
\paragraph{}
Le courant $i_c$ traversant dans le condensateur $C$ est égal à la différence entre le courant circulant dans l'inductance $L$ et le courant de sortie $i_s$ : $i_c = i_L - i_s$\\
Sa valeur moyenne est nulle comme nous l'avons montré précédemment.

\paragraph{}
Nous connaissons aussi la relation liant $v_s$, la tension aux bornes du condensateur $C$ à $q$, sa charge : \[Cv_s=q \]

\paragraph{}
L'ondulation de la tension de sortie prend donc pour expression :
\[ \Delta V_s = \frac{\Delta Q}{C} \]

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{fig.png}
\caption{\textit{Formes d'ondes}}
\end{minipage}
\end{figure}

\paragraph{}
Soit $\Delta Q$ la variation positive de la charge du condensateur $C$. On peut calculer géométriquement (cf. Figure $9$) $\Delta Q$ en remarquant qu'il s'agit de l'aire du triangle hachuré, dont la base vaut $\frac{T}{2}$ et la hauteur $\frac{\Delta I_L}{2}$.\\
On obtient alors : $\Delta Q = \frac{T\Delta I_L}{8}$ avec $T=\frac{1}{F}$.\\
d'où
\[ \Delta V_S = \frac{\Delta I_L}{8CF_{dec}} \]

\section{Étude du fonctionnement}
\subsection{Loi de commande}
Quelle soit élaborée de manière analogique ou numérique, le signal représentant la loi de commande, notée $f_m$ sur la figure $1$, peut se comprendre à l'aide des chronogrammes de la figure $10$. La comparaison des signaux $V_p(t)$ et $V_\alpha(t)$ permet de créer la fonction de modulation $f_m(t)$ dont le conditionnement (amplification, isolation) permettra le contrôle des interrupteurs de puissance, notés $T_1$ et $T_2$ (cf.Figure $2$), définissant leur temps de conduction, et la fréquence de découpage, assurant le séquencement du transfert de puissance.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{cmd.png}
\caption{\textit{Création du signal de commande}}
\end{minipage}
\end{figure}

\paragraph{}
Sur ce principe, un module de commande permet de générer un signal $V_{f_m(t)} \in [0;15V]$ image de la loi de commande directement interprétable par l'électronique de commande rapprochée du module de puissance. Ce module génère également un signal de commande complémentaire sur la période à l'image de $1-f_m(t)$, mais qui ne sera pas utilisé durant cette séance de TP.

\paragraph{}
Nous avons mesuré le signal de commande généré par le module de commande, et après avoir vérifié qu'il s'agissait bien d'un signal créneau d'amplitude $0 - 15V$, nous avons tracé pour une fréquence de découpage fixée, $F_{dec}=998,6Hz$, l'évolution de $\alpha$ en fonction de $V_\alpha$ (cf. Figure $11$).\\
Nous avons du, pour cela, utiliser une source de tension stabilisée externe pour générer la tension $V_\alpha$ entre $0V$ et $10V$ qui est la limite indiquée par le constructeur sur le module de commande.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{alpha.png}
\caption{\textit{Évolution de $\alpha$ en fonction de $V_\alpha$}}
\end{minipage}
\end{figure}

\paragraph{}
Alors que nous nous attendions à une relation linéaire entre nos deux grandeurs mesurées. Nous remarquons avec surprise l'existence de deux seuils :
\begin{itemize}
\item $\alpha=5\%$ pour $V_\alpha \in [0V;2,2V]$.
\item $\alpha=95\%$ pour $V_\alpha \in [9V;10V]$.
\end{itemize}

\paragraph{}
Cette non-linéarité, voulue par le concepteur, permet d'avoir en permanence du découpage et donc de garder le contrôle sur les grandeurs électriques du système.


\subsection{Étude de la structure non réversible en courant}
Dans cette partie, l'interrupteur $S_1$ est fermé et l'interrupteur $S_2$ est ouvert ($T_2$ bloqué) afin d'étudier la structure non réversible en courant. Nous pouvons alors  nous ramener à un schéma électrique équivalent du circuit étudié (cf. Figure $12$).

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=1]{img3.png}
\caption{\textit{Structure non réversible en courant}}
\end{minipage}
\end{figure}

\paragraph{}
Nous avons câblé la structure non réversible en courant, sur notre pallaisse, de la manière suivante (cf. Figure $13$).

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=1]{img6.png}
\caption{\textit{Cablage de la structure non réversible en courant}}
\end{minipage}
\end{figure}

\subsubsection{Tension et courant de sortie}
\paragraph{}
Nous avons relevé à l'oscilloscope la tension en sortie de la cellule de commutation ($v_{cell}$), et le courant traversant l'inductance ($i_L$), lorsque le système fonctionne en mode continu.

\paragraph{}
Nous avons alors fixé la fréquence de découpage à $20kHz$ et le rapport cyclique à $\alpha=37\%$.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.3]{osc1.png}
\caption{\textit{Mesure à l'oscilloscope : $v_{cell}$ (en bleu ciel), $i_L$ (en rose), $f_m$ (en bleu)}}
\end{minipage}
\end{figure}

\paragraph{La tension $v_{cell}$}
est un signal créneau d'amplitude $V_e=50V$ lorsque $t\in [0;\alpha T_{dec}]$ et d'amplitude nulle le reste de la période $T_{dec}$. Il s'agit donc bien du signal attendu et prédit par l'étude préparatoire.

\paragraph{Le courant $i_L$}
est ici mesuré à l'aide d'une sonde de courant $1A/V$. Il s'agit d'une grandeur périodique ondulant entre une valeur minimum atteinte tous les $nT_{dec}$ et une valeur maximum atteinte tous les $(n+\alpha)T_{dec}$. Ces portions de droites ascendantes puis descendantes étaient aussi décrites par notre études préliminaires.\\
Nous pouvons aussi noter que le courant $i_L$ ne s'annule pas, il s'agit alors d'un fonctionnement en mode de conduction continu.

\paragraph{}
Nous remarquons cependant beaucoup d'oscillations non prédis par notre modèle de notre étude préliminaire. Il s'agit d'éléments perturbateurs tel que des capacités parasites, des inductances parasites présentes dans le montage mais aussi dans les câbles utilisés pour les mesures.\\
Pour y remédier, nous avons utilisé les sondes de l'oscilloscope, et nous avons minimiser la taille des fils utilisés sur le montage et nous les avons torsadé pour limiter leur effet inductif.

\paragraph{}
Nous sommes alors placé à la fréquence de découpage $9,25kHz$, pour pouvoir observer le passage du mode continue à discontinu en ne faisant varier que le rapport cyclique $\alpha$.

\begin{figure}[H]
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=0.08]{IMG_1919.jpg}
\caption{\textit{Mode discontinu : $f_m$ (en bleu), $i_L$ (en jaune)}}
\end{minipage}
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=0.08]{IMG_1920.jpg}
\caption{\textit{Mode continu : $f_m$ (en bleu), $i_L$ (en jaune)}}
\end{minipage}
\end{figure}

\paragraph{}
Nous pouvons à l'aide des précédentes mesures, déterminer de façon indirecte la valeur de l'inductance.\\
En effet, comme nous l'avons vu dans l'étude préliminaire : \[ \Delta i_L = \frac{(V_e-V_s)\alpha}{LF_{dec}}=\frac{(1-\alpha)\alpha V_e}{LF_{dec}} \]
Comme $V_e=50V$, $\alpha=37\%\%$ et $F_{dec}=20kHz$, on obtient : \[ L=\frac{(1-\alpha)\alpha V_e}{\Delta i_L F_{dec}}=549\mu H \]

Cette valeur est légèrement supérieure à celle indiquée sur le composant, $L=500\mu H$.

\subsubsection{Ondulation du courant}
\paragraph{}
Nous avons pu mettre en évidence durant le travail de préparation que $\Delta I_L$ dépendait de deux paramètres, $F_{dec}$ et $\alpha$. Nous allons donc à présent montrer ces dépendances expérimentalement.
\[ \Delta I_L =\frac{(1-\alpha)\alpha V_e}{LF_{dec}} \]

\paragraph{Remarque :}
Nous nous sommes placés en mode AC pour supprimer la composante continue du signal $I_L$ et effectuer des mesures dans la pleine échelle de l'écran de l'oscilloscope, afin de diminuer les incertitudes de mesures. Bien que l'oscilloscope ne soit pas un appareil de mesure, il est pour nous la méthode la plus pratique pour mesurer des ondulations.

\paragraph{Influence de $F_{dec}$}
Nous fixons le rapport cyclique à $\alpha=49,11\%$, afin d'analyser l'influence de la fréquence de découpage sur l'ondulation du courant.

\begin{figure}[H]
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=0.35]{S1.png}
\caption{\textit{\textit{Évolution de $\Delta i_L$ en fonction de $F_{dec}$}}}
\end{minipage}
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=0.35]{S2.png}
\caption{\textit{\textit{Évolution de $\frac{1}{\Delta i_L}$ en fonction de $F_{dec}$}}}
\end{minipage}
\end{figure}

\paragraph{}
Nous constatons, sans surprise, que l'ondulation de courant évolue bien linéairement avec l'inverse de la fréquence de découpage (cf. Figure $18$). Ainsi pour une fréquence de découpage deux fois plus grande, l'ondulation de courant est deux fois plus petite. Notre analyse théorique prédisait bien la proportionnalité de $\Delta I_L$ avec $\frac{1}{F_{dec}}$.

\paragraph{Influence de $\alpha$}
Nous fixons, à présent, la fréquence de découpage à $F_{dec}=7,8kHz$, afin d'analyser l'influence du rapport cyclique sur l'ondulation du courant.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.7]{S3.png}
\caption{\textit{Évolution de $\Delta i_L$ en fonction de $\alpha$}}
\end{minipage}
\end{figure}

\paragraph{}
Nous constatons, de nouveau sans surprise, une évolution quadratique de l'ondulation de courant en fonction du rapport cyclique. C'est également ce que prédisait la théorie où $\Delta I_L$ était fonction de $\alpha (1-\alpha)$ comme nous l'illustre la courbe théorique de la Figure $19$.

\subsubsection{Ondulation de la tension de sortie}
\paragraph{}
Nous avons pu mettre en évidence durant le travail de préparation l'expression de l'ondulation de la tension de sortie. \[ \Delta V_s =\frac{\Delta I_L}{8CF_{dec}} \]

\paragraph{}
Nous avons donc observé à l'oscilloscope, pour plusieurs fréquences de découpage fixées, l'ondulation du courant $i_L$ et l'ondulation de la tension $v_s$.

\paragraph{Remarque :}
L'ondulation de tension étant très faible, il est difficile d'observer un signal non bruité à l'oscilloscope. Nous préférerons aussi utiliser un affichage en mode AC pour s'abstenir de la composante continue du signal qui ici ne nous intéresse pas.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{5k.png}
\caption{\textit{Évolution de $i_L$ (en rose) et de $v_s$ (en vert) au cours du temps pour $F_{dec}=5kHz$}}
\end{minipage}
\end{figure}

\paragraph{}
À la fréquence $F_{dec}=5kHz$, nous obtenons bien des formes d'onde attendues, le courant $i_L$ est triangulaire et la tension $v_s$ est quadratique.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{20k.png}
\caption{\textit{Évolution de $i_L$ (en rose) et de $v_s$ (en vert) au cours du temps pour $F_{dec}=20kHz$}}
\end{minipage}
\end{figure}

\paragraph{}
À la fréquence $F_{dec}=20kHz$, les hypothèses faites sur le comportement du condensateur ne sont plus valables. Pour les fréquences élevées, le condensateur adopte un comportement résistif d'où une forme d'onde, pour la tension $v_s$ triangulaire. Par ailleurs, le courant $i_L$ préserve son comportement en présentant une forme d'onde triangulaire.

\paragraph{Caractéristique de sortie du montage}
Nous savons que la tension de sortie, $V_s$, ne dépend pas du courant $I_s$ en conduction continue, puisque $V_s=\alpha V_e$. Tandis qu'en conduction discontinue cette dépendance existe, puisque : \[ V_s=\frac{V_e}{1+\frac{2LF_{dec}I_s}{\alpha^2 V_e}} \]

\paragraph{}
Afin de tracer l'évolution de $V_s$ en fonction de $I_s$, nous nous fixons $\alpha=12\%$ et faisons varier la charge en sortie du montage. Nous obtenons alors :

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{S10.png}
\caption{\textit{Évolution de $V_s$ e fonction de $I_s$}}
\end{minipage}
\end{figure}

\paragraph{}
Nous avons bien la présence des deux modes de conduction :
\begin{itemize}
\item En mode de conduction discontinue, la tension de sortie varie avec le courant de sortie.

\item En mode de conduction continue, on ne devrait pas avoir d'évolution de la tension de sortie avec le courant de sortie. Cependant on remarque une légère diminution de la tension lorsque le courant augmente.
\end{itemize}

\subsubsection{Association Hacheur--MCC}
\paragraph{}
Le filtre de sortie et la charge résistive sont remplacés par une machine à courant continue (MCC). La machine appairée servira de charge en la connectant au plan de charge de la table.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=1]{MCC_schema.png}
\caption{\textit{Schéma du montage}}
\end{minipage}
\end{figure}

\paragraph{Remarque :}
La tension d'alimentation du hacheur doit être ajustée pour être compatible à celle de la MCC.

\paragraph{}
Nous avons ainsi pu relever les formes d'onde à l'oscilloscope pour un point de fonctionnement en conduction continue.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.7]{MCC.png}
\caption{\textit{Mesure à l'oscilloscope des formes d'ondes : $f_m$ (en bleu), $V_{cell}$ (en bleu clair) et $I_{moteur}=I_L$ (en rose)}}
\end{minipage}
\end{figure}

\paragraph{}
En conduction continu, la MCC fournit les mêmes formes d'onde pour $V_{cell}$ et $I_L$ que ceux obtenus avec le filtre de sortie et la charge résistive.

\paragraph{Calcul de l'inductance de l'induit :}
Comme nous l'avons fait sur le montage précédent, nous allons chercher à exprimer à partir de nos mesure l'inductance d'induit de la MCC.\\
Avec $\alpha=20\%$, $\Delta i_L=0,9A$ et $F_{dec}=20kHz$, on obtient : \[ L=\frac{(1-\alpha)\alpha V_E}{\Delta i_L F_{dec}}=450 \mu H \]

\paragraph{Influence de $\alpha$ sur la vitesse de rotation :}
Nous nous sommes ensuite intéressés à l'évolution de la vitesse de rotation, notée $\omega$, de la MCC en fonction du rapport cyclique $\alpha$.

\paragraph{}
Nous avons mesuré, à l'osciloscope, le rapport cyclique et la tension issue de la génératrice tachymétrique, image de $\omega$ la vitesse de rotation de la MCC. Cette génératrice à un coefficient de conversion $1V$ pour $100tr/min$.

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{S4.png}
\caption{\textit{Évolution de $\omega$ en fonction de $\alpha$}}
\end{minipage}
\end{figure}

\paragraph{}
La vitesse du moteur évolue linéairement avec le rapport cyclique $\alpha$. Il donc un simple coefficient de  entre ces deux grandeurs qui est : \[ K=0,06rad/s/\% \]

\paragraph{Visualisation du point de fonctionnement de la MCC :}
Nous pouvons visualiser à l'oscilloscope le point de fonctionnement dans le plan "couple-vitesse" puisque le couple est proportionnel à $I_s$ et la vitesse à $V_s$. Ainsi en affichant en mode $XY$ le courant $I_s$ et la tension $V_s$, on visualise le point de fonctionnement dans le plan voulu.

\begin{figure}[H]
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=0.5]{Q1.png}
\caption*{Au repos}
\end{minipage}
\begin{minipage}[b]{0.45\textwidth}
\centering
\includegraphics[scale=0.5]{Q2.png}
\caption*{En fonctionnement}
\end{minipage}
\caption{\textit{Point de fonctionnement de la MCC dans le plan "couple-vitesse"}}
\end{figure}

\paragraph{}
Au repos le point de fonctionnement est sur l'origine du repère, il n'y a ni couple ni vitesse. Avec le fonctionnement un quadrant (structure non réversible en courant), lorsque la MCC est en marche, le point de fonctionnement est dans le quadrant supérieur droit correspondant à une vitesse et un couple positifs.

\paragraph{Remarque :}
Si nous faisons chuter brutalement le rapport cyclique $\alpha$, nous abaissons brutalement la tension de la MCC. Celle-ci ayant de l'inertie va se comporter momentanément comme un générateur. Le point de fonctionnement va donc vouloir sortir du quadrant supérieur droit pour passer dans celui inférieur droit, autorisant un couple négatif et une vitesse positive. Cependant notre structure n'étant pas réversible en courant, le point de fonctionnement ne pourra sortir de ce quadrant supérieur droit.


\subsection{Étude de la structure réversible en courant}
\paragraph{}
Dans cette partie l'interrupteur $S_2$ est fermé (position C), l'interrupteur $T_2$ est commandé. Le hacheur est connecté sur la cellule de filtrage initiale.

\paragraph{}
Pour $\alpha=21\%$, $F_{dec}=25,14kHz$, nous pouvons mettre en évidence la réversible instantanée en courant de la structure (cf. Figure $27$).

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.2]{IMG_1921.jpg}
\caption{\textit{Mesure à l'oscillocope de $v_{cell}$ (en jaune), de $i_L$ (en bleu) et de $v_s$ (en vert)}}
\end{minipage}
\end{figure}

\paragraph{}
Les mesures, effectuées à l'oscilloscope de la Figure $27$, mettent en évidence le fonctionnement réversible en courant de la structure. Nous pouvons résumer son fonctionnement de la manière suivante. (cf. Figure $1$)
\begin{itemize}
\item Lorsque le courant $i_L$ est croissant et positif, l'interrupteur $T_1$ conduit. 
\item Lorsque le courant $i_L$ est décroissant et positif, la diode $D_2$ conduit. 
\item Lorsque le courant $i_L$ est décroissant et négatif, la diode $D_1$ conduit. 
\item Lorsque le courant $i_L$ est croissant et négatif, l'interrupteur $T_2$ conduit. 
\end{itemize}

\paragraph{}
Les oscillations, que nous observons, sont encore une fois des perturbations induites par des capacités et des inductances parasites internes aux composants physiques constituant notre montage.

\paragraph{}
Nous regrettons, cependant, ne pas avoir eu le temps durant la séance de TP d'effectuer des relevés expérimentaux suffisant pour pouvoir tracer la caractéristique de sortie du hacheur $(V_s,I_s)$ pour la valeur de $\alpha$ précédente, assurant en fonctionnement réversible de la structure.

\subsection{Étude de la structure de hacheur 4 quadrants}
\paragraph{}
La structure de hacheur $4$ quadrants peut être schématisée de la manière suivante. (cf. Figure $28$)

\begin{figure}[H]
\begin{minipage}[b]{\textwidth}
\centering
\includegraphics[scale=0.5]{4quadrant.png}
\caption{\textit{Schématisation d'une hacheur $4$ quadrants avec en sortie une MCC}}
\end{minipage}
\end{figure}

\paragraph{Remarque :}
La tension d'alimentation du hacheur doit être ajustée pour être compatible à celle de la machine à courant continu.

\paragraph{}
Nous avons pu voir les aspects de cette structure $4$ quadrant avec monsieur GABSI mais nous n'avons pas eu le temps de reproduire la manipulation et d'effectuer les mesures souhaitées.


\section{Conclusion}
Jusqu'à présent, nous avions étudié que le fonctionnement d'hacheurs non réversibles. Mais le convertisseur sur lequel nous venons de travailler présente, en pratique, un avantage notable lorsqu’ils sont destinés à contrôler le fonctionnement d’une machine à courant continu (MCC).\\
En effet, il permet son fonctionnement à la fois en moteur et en génératrice.
Dans la plupart des cas, il faut permettre à la MCC de fonctionner au moins dans 2 quadrants, selon que l’on a $I > 0$ et $E > 0$ (moteur), ou $I < 0$ et $E > 0$ (génératrice), ce qui est rendu possible grâce à la structure réversible en courant étudiée ici.

\end{document}