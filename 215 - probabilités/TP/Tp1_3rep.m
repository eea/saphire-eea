clearvars;
close all;

load DataTP

figure(1);
subplot(2, 2, 1);
imagesc(s);
subplot(2, 2, 2);
imagesc(r1);
subplot(2, 2, 3);
imagesc(r2);
subplot(2, 2, 4);
imagesc(r3);

r = round((r1 + r2 + r3) / 3);
figure(2);
imagesc(r);

nbBit = size(s, 1) * size(s, 2);
nbErreur(1) = sum(sum(abs(r1 - s)));
nbErreur(2) = sum(sum(abs(r2 - s)));
nbErreur(3) = sum(sum(abs(r3 - s)));
nbErreur(4) = sum(sum(abs(r - s)));
nbErreur(5) = sum(nbErreur(1:3));

alpha = [0, 0, 0, 0, 0];
beta = [0, 0, 0, 0, 0];
esperance = [0, 0, 0, 0, 0];
variance = [0, 0, 0, 0, 0];
for i = 1:5
    alpha(i) = nbErreur(i) + 1;
    if (i == 5)
        beta(i) = 3 * nbBit - nbErreur(i) + 1;
    else
        beta(i) = nbBit - nbErreur(i) + 1;
    end
    esperance(i) = alpha(i) / (alpha(i) + beta(i));
    variance(i) = alpha(i) * beta(i) / ((alpha(i) + beta(i))^2 * (alpha(i) + beta(i) + 1));
end
