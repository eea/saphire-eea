clearvars;
close all;

load DataTP
load DataCompTP

figure(1);
subplot(5, 2, 1);
imagesc(s);
subplot(5, 2, 2);
imagesc(r1);
subplot(5, 2, 3);
imagesc(r2);
subplot(5, 2, 4);
imagesc(r3);
subplot(5, 2, 5);
imagesc(r4);
subplot(5, 2, 6);
imagesc(r5);
subplot(5, 2, 7);
imagesc(r6);
subplot(5, 2, 8);
imagesc(r7);
subplot(5, 2, 9);
imagesc(r8);
subplot(5, 2, 10);
imagesc(r9);

r = round((r1 + r2 + r3 + r4 + r5 + r6 + r7 + r8 + r9) / 9);
figure(2);
imagesc(r);

nbBit = size(s, 1) * size(s, 2);
nbErreur(1) = sum(sum(abs(r1 - s)));
nbErreur(2) = sum(sum(abs(r2 - s)));
nbErreur(3) = sum(sum(abs(r3 - s)));
nbErreur(4) = sum(sum(abs(r4 - s)));
nbErreur(5) = sum(sum(abs(r5 - s)));
nbErreur(6) = sum(sum(abs(r6 - s)));
nbErreur(7) = sum(sum(abs(r7 - s)));
nbErreur(8) = sum(sum(abs(r8 - s)));
nbErreur(9) = sum(sum(abs(r9 - s)));
nbErreur(10) = sum(sum(abs(r - s)));
nbErreur(11) = sum(nbErreur(1:9));

alpha = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
beta = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
esperance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
variance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
for i = 1:11
    alpha(i) = nbErreur(i) + 1;
    if (i == 11)
        beta(i) = 9 * nbBit - nbErreur(i) + 1;
    else
        beta(i) = nbBit - nbErreur(i) + 1;
    end
    esperance(i) = alpha(i) / (alpha(i) + beta(i));
    variance(i) = alpha(i) * beta(i) / ((alpha(i) + beta(i))^2 * (alpha(i) + beta(i) + 1));
end
