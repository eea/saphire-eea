#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 20:04:59 2018

@author: gael
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from math import *
from cmath import *

g_m = 22.4e-3
R_D = 1000
R_S = 1000
C_S = 37.2e-6
C_i = 31e-9
R_12 = 41.5e3
R_e = 10000


def H(w):
    H1 = -g_m*R_D/(1+g_m*R_S)
    H2 = (1+R_S*C_S*1j*w)/(1+(R_S*C_S*1j*w)/(1+g_m*R_S))
    H3 = (R_12*C_i*1j*w)/(1+(R_12+R_e)*C_i*1j*w)
    return H1*H2*H3

def H2(w):
    H1 = -g_m*R_D/(1+g_m*R_S)
    if 1 >= np.abs(R_S*C_S*1j*w):
        H2 = 1
    else:
        H2 = R_S*C_S*1j*w
    if 1 >= np.abs(R_S*C_S*1j*w)/(1+g_m*R_S):
        H3 =1
    else:
        H3 = (R_S*C_S*1j*w)/(1+g_m*R_S)
    H4 = (R_12*C_i*1j*w)
    if 1 >= abs((R_12+R_e)*C_i*1j*w):
        H5 = 1
    else:
        H5 = (R_12+R_e)*C_i*1j*w
    return H1*H2*H4/(H3*H5)


def G(w):
    return 20*np.log10(abs(H(w)))

def G2(w):
    return 20*np.log10(abs(H2(w)))

def phi(w):
    return np.angle(H(w))*180/np.pi

def phi2(w):
    return np.angle(H2(w))*180/np.pi

np.vectorize(H)
np.vectorize(G)
np.vectorize(phi)
np.vectorize(H2)
np.vectorize(G2)
np.vectorize(phi2)

W = 10**np.linspace(-2,6,1001)
F = W/(2*np.pi)

G2_W = [G2(w) for w in W]
phi2_W = [phi2(w) for w in W]

plt.figure()
plt.xlim(0,100000)
plt.xlabel('Fréquence [Hz]')
plt.ylim(-60,30)
plt.ylabel('Gain [dB]')
plt.plot(F,G(W))
plt.plot(F,G2_W)
plt.grid()

plt.figure()
plt.xlim(0,100000)
plt.xlabel('Fréquence [Hz]')
plt.ylim(-180,0)
plt.ylabel('Phase [°]')
plt.plot(F,phi(W))
plt.plot(F,phi2_W)
plt.grid()
