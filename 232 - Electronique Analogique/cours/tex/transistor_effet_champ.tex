\documentclass[../232.tex]{subfiles}
\begin{document}
\section{Le transistor à effet de champ} %F
\subsection{Rappels sur les transistors à effet de champs}
\subsubsection{Idée générale}
L’idée générale dans un transistor à effet de champ est de moduler par l’application d’un potentiel de grille (créant un champ électrique) la conductivité d’un canal de conduction reliant les deux électrodes de source et drain. Les transistors à effet de champ sont désignés par le sigle TEC en
français (Transistor à Effet de Champ) et FET en anglais (Field Effect Transistor) que nous garderons par anglicisme. Deux types de transistors à effet de champ existent : les transistors à grille isolée (Métal Oxyde Semi-conducteur FET) et les transistors à jonction (JFET). Pour le premier type le champ électrique est créé par la différence de potentiel entre grille et canal, pour le second c’est la
polarisation inverse de la jonction PN grille canal qui module l’épaisseur de la zone de charge d’espace et par la même l’épaisseur du canal.

\paragraph{Idée du transistor JFET normally on à canal n}

\begin{figure}
	\centering
	\includegraphics[height = 5cm]{jfet}
	\caption{Schéma de principe d'un transistor JFET normally on à canal n}
\end{figure}

\begin{itemize}
	\item Plus la jonction PN est polarisée en inverse, plus la largeur du canal est réduite.
	\item  Ce type de transistor est aussi appelé \emph{transistor à appauvrissement}
	\item Sans action sur la grille, la conduction est possible (d'où le "normally on"). On ne peut donc réduire que la conduction.
\end{itemize}
\pagebreak
\subsubsection{"Normally "On" et Normally Off, canal N et P}

Le courant qui circule dans le canal est donné par $I=qnv\delta Z$ où $v$ est la vitesse des porteurs qui dépend
de la tension appliquée entre la source et le drain et n la densité volumique de porteurs.
Si on veut moduler le courant par une tension extérieure, il faut que celle-ci agisse, soit sur l'épaisseur
du canal$\delta$ , soit sur la densité surfacique de charge dans le canal $n\delta$. Nous aurons donc deux grandes
classes de transistors à effet de champ:
\begin{enumerate}
	\item Les transistors à effet de champ dans lesquels la tension extérieure module l'épaisseur $\delta$ du canal.
	Cette modulation résulte de la variation de l'épaisseur d'une zone de charge d'espace sous l'action de la
	tension de polarisation. La zone de charge d'espace peut être:
	\begin{itemize}
		\item Celle d'une jonction PN polarisée en inverse et on a les JFET (Junction Field Effect
		Transistor);
		\item Celle d'une diode Schottky polarisée en inverse et on a les MESFET (MEtal
		Semiconducteur Field Effect Transistor);
		\item Celle d'une structure MOS (Métal Oxyde Semiconducteur) polarisée en régime de
		désertion et on a les MOSFET à désertion (ou déplétion).
	\end{itemize}
Pour ce type de transistors, le canal existe même en l'absence de polarisation sur la grille. Ils sont alors qualifiés de "\textbf{Normally On}". Une polarisation dans le sens convenable pince le canal, ce qui a pour effet de diminuer son épaisseur et donc le courant entre la source et le drain. Ce sont les FET à désertion, ou à \textbf{appauvrissement}
\item Les transistors à effet de champ dans lesquels la tension extérieure module la densité surfacique de porteurs (produit de la densité volumique de porteurs dans le canal par son épaisseur). Cette modulation résulte de la variation de charges sur l'armature d'un condensateur sous l'action de la tension de polarisation. Le condensateur peut être:
	\begin{itemize}
	\item Une structure MOS (Métal Oxyde Semiconducteur) polarisée en régime d'inversion et on a les MOSFET à enrichissement;
	\item Une structure Métal-Semiconducteur-Semiconducteur et on a les HEMT (High Electron
	Mobility Transistor) ou TEGFET (Two dimensional Electron Gas Field Effect Transistor).
	\end{itemize}
Pour ce type de transistors, en l'absence d'une polarisation sur la grille le canal n'existe pas. Ils sont
alors qualifiés de "Normally Off". Une polarisation dans le sens convenable induit un canal, ce qui a
pour effet de permettre le passage du courant entre la source et le drain. Ce sont les FET à
enrichissement.
\end{enumerate}

Dans ce module, on se limitera à l'étude des MOSFET à enrichissement ou Normally Off. Comm pour le transistor bipolaire, l'étude complète du transistor ne peut se faire analytiquement en raison des effets bi-dimensionnels. La coupe d'un MOSFET à enrichissement est représentée sur la figure suivante. Nous traiterons le problème en découplant l'effet de la tension appliquée sur la grille (direction du champ résultant suivant 0y) de celui de la tension appliquée entre la source et le drain (direction du champ résultant suivant 0x). La nature du canal de conduction permet de distinguer les
transistors à effet de champ à canal N et à canal P, ce qui ajoute à la diversité des composants rencontrés. L’étude suivante est focalisée sur un transistor à enrichissement à canal N.

\pagebreak
\subsubsection{Fonctionnement phénoménologique}

Un MOSFET à enrichissement à canal N est constitué de deux zones très dopées de type N (la source et le drain) réalisées dans un substrat de type P sur lequel on a déposé un oxyde et une grille métallique (voir figure a). Nous allons analyser le fonctionnement du transistor lorsque le substrat est au même potentiel que la source, pris comme potentiel de référence.


\paragraph{Remarque } $I_G = 0$  ( car la grille est isolé) en régime permanent. et par conséquent $I_S =I_D$ en régime permanent (pas d'accamulation de charge)
\begin{enumerate}[label=(\alph*)]
	\item Tant que la tension appliquée sur la grille est insuffisante pour induire une couche d'inversion à la surface du semiconducteur $(V_GS<V_T)$, si on polarise le drain, aucun courant ne peut circuler car on a deux jonctions PN tête-bêche dont l'une est en inverse. Le transistor est bloqué (cas de la figure a). On peut noter la présence d'une zone de charge d'espace autour des deux jonctions PN.
	\item Si on applique sur la grille une tension supérieure à la tension de seuil, on induit une couche 	d'inversion (le canal) à la surface du semiconducteur (figure b) : lorsque la polarisation appliquée sur la grille est supérieure à une certaine tension appelée tension de seuil et notée $V_T$, non seulement on repousse les trous (zone de charge d'espace), mais de plus on induit une couche d'inversion riche en électrons près de la surface du semiconducteur. Nous sommes en régime d'inversion. On définira ce régime d'inversion comme celui pour lequel la densité volumique d'électrons en surface $(n_s)$ est
	supérieure à la densité volumique de trous dans le semiconducteur à l'équilibre $(p_v)$. Ici la couche d'inversion est crée par diffusion des électrons depuis les zones de source et de drain. Ce phénomène est très rapide et l'établissement de la couche d'inversion peut se faire en quelques picosecondes, voire moins.
	\item Une polarisation $V_{DS}$ appliquée sur le drain crée alors un flux d'électrons de la source vers le drain et il en résulte un courant de drain $I_D$. On peut remarquer que la tension dans le canal est plus faible du côté du drain que du côté de la source, ce qui conduit à une diminution de la densité surfacique d'électrons dans le canal depuis la source jusqu'au drain (illustration figure c).
	\item Lorsque la tension appliquée sur le drain conduit à une valeur $V_{GS}-V_{DS}$ égale à la tension de seuil, le canal est coupé au niveau du drain (figure d).
	\item Si on continue à augmenter la tension de drain, on a un fort champ électrique entre la fin du canal et le	drain qui catapulte les électrons vers le drain (figure e). La tension sur le canal ouvert devient	constante et égale à $V_{DS,sat}=(V_{GS}-V_T)$. Si on suppose que la position du point de pincement varie	peu, le courant sature à une valeur $I_{Dsat}$.
\end{enumerate}


\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{../img/MOSFET_5}
	\caption{Fonctionnemment d'un transistor MOSFET}
	\label{fig:mosfet5}
\end{figure}


L'allure du réseau de caractéristiques de drain est reportée sur la figure suivante. Ici la tension de seuil
$V_T$ est positive.
\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{../img/id_vds}
	\caption{caractéristique au Drain}
	\label{fig:idvds}
\end{figure}

\subsubsection{Relation fonctionnelles}
	Le potentiel de la source est pris comme potentiel de référence. Les potentiels respectifs sur la grille, sur le drain et sur le substrat sont notés $V_GS$, $V_DS$ et $V_BS$. Le potentiel dans le canal à une abscisse $x$ le long de l'interface silicium-oxyde est notée $V(x)$. On traite du cas d'un transistor à canal N, c'est à dire avec un substrat de type P.

\subsubsection{Calcul du courant en régime ohmique}
	
	Nous sommes en mesure d'établir la relation entre les tensions appliquées sur le drain, la grille et le substrat et le courant circulant dans le canal lorsque celui-ci est ouvert. Le courant ID dans le canal estconstant.
	
	Connaissant $\delta(x)$ l'épaisseur du canal et $Z$ la largeur du transistor (dimension du canal parallèlement à l'interface dans la direction perpendiculaire à la direction source-drain), on peut calculer le courant. En prenant le sens positif pour le courant du drain vers la source, on a:
	\[ 
	I_{D} = qn_xv_n(x)\delta(x)Z 
	\]
	
	Si on suppose que l'on est en régime de mobilité, la vitesse des électrons est proportionnelle au champ électrique, en notant $\mu_n$ la mobilité des porteurs de charges on a:
	\[
	 v_n(x) = - \mu_n E(x) = \mu_n \deriv[V]{x}
	\]
	On introduit $Q_i(x)= q n(x) \delta(x)$. Et $C_{ox} =\frac{\epsilon_{ox}\epsilon_0}{e_{ox}}$ , capacité par unité de surface de la couche d'oxyde (ou $e_{ox}$ est l'épaisseur du canal)
	
	On a donc en un point $C$ quelconque du canal:
	
	\[
	Q_i(x) = -C_{ox}(V_{GC}(x)-V_T)
	\]
	En décomposant $V_{GC} =V_{GS}-V(x)$, où $V(x)$ est la tension entre un point $C$ en $x$ dans le canal et la source. On en déduit l'ED sur le courant :
	
	\[
	I_D = \mu_n Z C_{ox} (V_{GS}-V_T -V(x))\deriv[V]{x}
	\]
	
	On intègre par séparation des variables entre la source $V=0, x=0$ et le drain $V=V_{DS}, x=L $ on a alors :
	\[ 
	I_D = \frac{\mu_n Z C_{ox}}{L}\left[(V_{GS}-V_T)V_{DS}-\frac{V_{DS}^2}{2}\right]
	\]
	
	Pour établir l'équation, nous avons supposé que le canal existe partout et que l'on est en régime de
	mobilité. Nous allons mieux préciser ce que cela suppose. La couche d'inversion doit exister tout au
	long du canal.
	Cela implique que $Q_i$ est partout négatif. La tension V est maximale au niveau du drain et vaut $V_DS$. La condition pour un canal partout ouvert est donc:
	\[
	V_{DS} \leq (V_{GS}-V_T)
	\]
	
	\subsubsection{Saturation du courant par pincement}
	
	Lorsque la condition définie par l'équation 8 n'est plus vérifiée, c'est à dire lorsque la tension de drain
	est supérieure à VDSsat définie par:
	$V_{DSsat} = V_{GS}-V_T$
	Le canal est pincé au niveau du drain. Noius entrons alors dans un régime de saturation du courant (étape (e) vue précédement). L'expression du courant est alors 
	\[
	I_{Dsat} = \frac{\mu_n Z C_{ox}}{2L}(V_{GS}-V_T)^2
	\]
	On défini alors la transconductance définie par:
	\[
	g_m = \deriv[I_{Dsat}]{V_{GS}} = \frac{\mu_nZC_{ox}}{L}(V_{GS}-V_T)
	\]
	
	La commande du courant de saturation est quadratique en $V_{GS}-V_T$.
	
	\subsection{Caractéristique statique}
	De part sa constitution (grille isolée) et son utilisation (jonction polarisée en inverse), le courant de
	grille d’un transistor à effet de champ pourra toujours être négligé vis-à-vis des autres courants d’un
	montage (en régime statique). On retiendra donc $I_G=0$.
	
	Le comportement d’un transistor à effet de
	champ est assez bien décrit en régime de saturation (appelé aussi zone source de courant) par la
	relation quadratique liant le courant de drain à la tension $V_{GS}$:
	\[I_D= I_{DSS}\left(1-\frac{V_{GS}}{V_T}\right)^2\]
	Avec $I_{DSS}$ le courant de drain a tension $V_{GS}$ nulle et $V_{T}$ la tension de seuil ou de pincement (JFET). Ces deux grandeurs sont caractéristique du composant et sont fortement variable d'un composant à l'autre (dispersion importante). Le tracé suivant illustre cette loi de comportement.
	
	\begin{figure}
		\centering
		\includegraphics[width=0.7\linewidth]{BF245.png}
		\caption{Extrait de la datasheet du BF245}
		\label{fig:bf245}
	\end{figure}
	
	La caractéristique $I_D(V_{DS})$ laisse apparaitre deux zones distinctes. Une première à $V_{DS}$ faible où le comportement du composant semble linéaire, la pente des courbes liant courant et tension étant paramétrée par $V_{GS}$ . Cette zone est appelée zone ohmique, elle donnera lieu à l’utilisation du composant comme\textit{ résistance commandable en tension}. Après une zone coudée, le comportement du transistor devient assimilable à une source de courant paramétrée par $V _{GS}$ . Cette région porte le nom de \textit{zone pincée ou zone de saturation}, ou encore zone source de courant, et sera la zone privilégiée de fonctionnement du composant dans un \textit{montage amplificateur}. On notera que zone ohmique et zone pincée sont délimitées par la courbe vérifiant : $ V_{DS} =V_{GS}-V_T$

\subsection{Modèle du transistor à effet de champ}
	
	Dans la zone pincée et en basses fréquences, un schéma équivalent valable en petits signaux permet de rendre compte du comportement de source de courant commandée du transistor. Le modèle présenté est valable autour d’un point de fonctionnement pour de petites excursions. L’imperfection de la source de courant est retranscrite par la résistance $r_o$ . L’influence de cette dernière pourra souvent être négligée vis-à-vis des autres résistances du montage.
	\begin{figure}[hbt]
		\centering
		\begin{subfigure}{0.5\linewidth}
			\vspace{2em}
			\begin{circuitikz}
				\draw (0,2)node[left]{G} --(1,2) to[open, v_<=$V_{GS}$,] (1,0) -- (4.5,0) to[R,l_=$r_{o}$] (4.5,2);
				\draw (3.5,0) to[I,l=$g_mV_{GS}$,i<=$ $] (3.5,2) -- (5.5,2) node[right]{D};
				\draw (2,0) --(2,-1) node[below]{S};
			\end{circuitikz}
			\caption{Modèle Basse Fréquence et petit signaux}
		\end{subfigure}%
		\begin{subfigure}{.5\linewidth}
			\begin{circuitikz}
				\draw (0,2)node[left]{G} --(1,2) to[C, l_=$C_{GS}$,] (1,0) -- (4.5,0) to[R,l_=$r_{o}$] (4.5,2);
				\draw (3.5,0) to[I,l=$g_mV_{GS}$,i<=$ $] (3.5,2) -- (5.5,2) node[right]{D};
				\draw (2,0) --(2,-1) node[below]{S};
				\draw (1,2) to[C,l=$C_{GD}$] (3.5,2);
			\end{circuitikz}
			\caption{Modèle Haute fréquence}
		\end{subfigure}
	\end{figure}
	
	En hautes fréquences (en limite haute de la bande passante d’un montage amplificateur) le fonctionnement du composant est altéré par les capacités parasites. On notera concernant la capacité grille source d’un transistor MOS qu’il s’agit d’un véritable condensateur physique. D’après les
	définitions précédentes, on a $C_{GS} \propto C_{ox}ZL$.
	
	Comme pour le transistor bipolaire cherchons la fréquence à laquelle le gain en courant du transistor à effet de champ est unitaire.
	
	\[ f_T = f | |\beta| = 1 \text{ avec } \beta= \left.\frac{i_d}{ig}\right|_{V_{ds}=0}\]
	On trouve $\beta(\omega) = \frac{g_m}{jC_{gs}\omega}$ et $f_t = \frac{g_m}{2\pi C_{GS}}$ 
	
	Soit en remplacant dans l'expression les termes $g_m$ et $C_{GS}$ :
	\[
	f_T = \frac{1}{2\pi} (V_{GS}-V_T)\frac{\mu_n}{L^2}
	\]
	
	On a donc tout intérêt à avoir la mobilité la plus élevée (canal N plutôt que P) et une faible longueur de
	grille (évolutions technologiques).


\end{document}