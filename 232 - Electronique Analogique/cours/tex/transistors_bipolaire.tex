\documentclass[../232.tex]{subfiles}
\begin{document}
\section{Le transistor bipolaire}
Un transistor bipolaire est constitué de deux jonctions PN montées têtes bêches. Deux sortes de transistors bipolaires existent donc : les PNP et les NPN. Le semi-conducteur de la région centrale est connecté à l’électrode nommée base (B), les deux autres électrodes étant nommées émetteur (E) et
collecteur (C). On notera que ces deux électrodes ne sont pas interchangeables, le dopage de l’émetteur étant plus important que celui du collecteur. Sans rentrer dans la physique des semi-conducteurs, pour un transistor NPN le fonctionnement du composant peut être réduit pour une première étude au raisonnement suivant. En régime de fonctionnement normal dans la zone active, la jonction BE est polarisée en direct et BC en inverse. Un courant d’électrons (majoritaires dans E)
s’établi de l’émetteur vers la base (de même qu’un courant de trous s’établi de la base vers l’émetteur). Ces électrons (respectivement trous), lorsqu’ils arrivent au voisinage de la zone de charge d’espace de la jonction BC sont happés par le fort champ électrique qui y règne. Un courant peut s’établir dans la jonction BC pourtant bloquée :

 \emph{c’est l’effet transistor. Ce courant de collecteur est principalement réglé par le courant $i_B$.}
\begin{figure}[hbt]
	\centering
	\includegraphics[width=0.7\linewidth]{NPN_PNP.png}
	\caption{}
	\label{fig:npnpnp}
\end{figure}

Il faut noter que les deux porteurs de charge (électrons et trous) participent à
ce courant, d’où le nom de transistor bipolaire.
\subsection{Régime de fonctionnement}
Les jonctions émetteur-base et base-collecteur peuvent être polarisées soit en direct, soit en inverse.
Suivant les cas, on défini quatre régimes de fonctionnement décrits dans le tableau ci-dessous.
\begin{center}
	\begin{tabular}{|c|c|c|}
		\hline 
		& jonction B/E & jonction B/C \\ 
		\hline 
		normal direct & direct & inverse \\ 
		\hline 
		normal inverse & inverse & direct \\ 
		\hline 
		saturé & direct & direct \\ 
		\hline 
		bloqué & inverse & inverse \\ 
		\hline 
	\end{tabular} 
\end{center}
\subsection{Caractéristique statiques}
Six grandeurs électriques permettent de décrire complètement le fonctionnement d’un transistor : les
tensions $V_{BE}$ $V_{BC}$ $V_{CE}$ , et les courants $I_B$ $I_C$ et $I_E$ . La loi des mailles et la loi des nœuds permettent de lier ces grandeurs (leur somme est nulle), donc quatre signaux sont indépendants et suffisent à décrire
l’état du composant. En réalité, les lois de la physique des semi-conducteurs établissent des relations
entre ces variables. On retiendra par exemple que la jonction base émetteur, qui se comporte comme une diode, est régie par l’équation : $I_B = I_s \exp(V_{BE}/V_T)$ avec $I_S$ le courant de saturation de la diode BE  et $V_T = \frac{kT}{q}$ environ égal à 25mV à la température ambiante.

Il est de coutume de représenter ces lois d’évolution par des réseaux de courbes paramétrées. La
figure ci-dessous représente l’un de ces réseaux pour des grandeurs continues (au sens temporel du
terme), appelé \emph{caractéristiques statiques du transistor}.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{caracteristique_statique_transistor.png}
	\caption{}
	\label{fig:caracteristiquestatiquetransistor}
\end{figure}

Première remarque sur la caractéristique $I_B (V_{BE})$ : on retrouve le comportement de diode. Le domaine
$I_C(V_{CE})$ peut être divisé en une région ou le transistor est dit bloqué (très faible courant de collecteur),
une zone de saturation (faible tension $V_{CE}$ ) et une zone ou le comportement est assimilable à une source de courant commandée par le courant de base, ou zone de fonctionnement linéaire utilisée en amplification. Pour être complet on notera qu’une hyperbole de puissance maximale dissipable limite cette région.
\subsection{Modèle(s) du transistor bipolaire}
Comme la diode, le transistor bipolaire peut être modélisé à différents niveaux de finesse. Le premier modèle, à l’ordre 0, considère le composant comme un simple amplificateur de courant. Ce modèle est suffisant pour prévoir la polarisation du montage. On écrira alors : $I_c=\beta I_B$ avec $\beta$ le gain en courant (aussi appelé parfois $H_fe$). La jonction BE est modélisée à l’ordre 0 par une source de tension de fem ~ 0,6V. Ce modèle est insuffisant pour l’étude du comportement dynamique du circuit pour plusieurs raisons. D’abord, le paramètre $\beta$ est fortement variable d’un composant à l’autre (pour un même modèle de transistor). Une plage de variation de 100 à 200 est courante. Ensuite, il s’agit d’un paramètre statique, le « gain en courant » dynamique est différent. Enfin, ce paramètre n’est pas constant, la jonction BE ayant en réalité un comportement de jonction de diode.

Pour le régime de fonctionnement dynamique, il est nécessaire de disposer d’un modèle plus précis.
Plusieurs modèles sont disponibles. Certains sont valables sur toute la caractéristique de fonctionnement (Ebers et Moll par exemple, réservé à la BF), d’autres, que nous allons utiliser, sont adaptés à l’étude en petits signaux, et valables sur une large gamme de fréquence lorsqu’ils sont complets.
D’autres modèles enfin considèrent le transistor dans son circuit comme un quadripôle, et se basent sur le formalisme matriciel pour décrire le fonctionnement du circuit. En particulier, les paramètres hybrides (h parameters) du transistor pour une configuration donnée (émetteur commun
...) sont utilisés dans le domaine des hautes fréquences.

Lors de l’étude basse fréquence d’un montage amplificateur, le schéma équivalent en petits signaux
suivant pourra être substitué au transistor. Il est important d’être bien conscient des limites de ce
modèle. En particulier, il ne devra pas être utilisé pour une étude en grands signaux, et il ne permet pas
en l’état de prévoir les limites hautes de la bande passante d’un circuit.
\begin{center}
\begin{figure}
	\begin{subfigure}{.6\linewidth}
		\begin{circuitikz}
			\draw (0,2)node[left]{B} --(1,2) to[R, l_=$r_{BE}$] (1,0) -- (4.5,0) to[R,l_=$r_{CE}$] (4.5,2);
			\draw (3.5,0) to[I,l=$g_mV_{BE}$,i] (3.5,2) -- (5.5,2) node[right]{C};
			\draw (2,0) --(2,-1) node[below]{E};
		\end{circuitikz}
	\end{subfigure}%
	\begin{subfigure}{.4\linewidth}
		\begin{minipage}[c]{0.8\linewidth}
		Ordre de grandeurs:
		\begin{itemize}
			\item $g_m \sim 100mS =100mA/V$ 
			\item $r_{BE} \sim 1k\Omega $
			\item $r_{CE} \sim 100k\Omega $
		\end{itemize}
		\end{minipage}
	\end{subfigure}
\end{figure}
\end{center}

Pour une étude en hautes fréquences, il faut compléter ce schéma avec les capacités parasites
(capacités de diffusion des jonctions essentiellement) du composant. La résistance parasite d’accès à la
base r BB’ (résistance du matériau semi conducteur) est également insérée. Suivant la gamme de
fréquence de travail, les inductances parasites de la connectique du composant pourront être ajoutées.
Le schéma équivalent petits signaux HF, appelé modèle de Giacoletto, est représenté ci-dessous.
\begin{center}
	\begin{figure}[hbt]
		\begin{subfigure}{.6\linewidth}
			\begin{circuitikz}
				\draw (-2,2)node[left]{$B$} to[R, l=$r_{BB'}$] (0,2) node[above right]{B'} --(1,2) to[R] (1,0) -- (4.5,0) to[R,l_=$r_{CE}$] (4.5,2);
				\draw (3.5,0) to[I,l=$g_mV_{BE}$,i] (3.5,2) -- (5.5,2) node[right]{C};
				\draw (2,0) --(2,-1) node[below]{E};
				\draw (0,2) to[C, l_=$C_{B'E}$] (0,0) --(1,0);
				\draw (0,2) -- (0,3) to[C, l=$C_{B'C}$] (4.5,3)--(4.5,2);
			\end{circuitikz}
		\end{subfigure}~ 
		\begin{subfigure}{.4\linewidth}
			\centering
			\begin{minipage}[c]{.9\linewidth}
				Ordre de grandeurs:
				\begin{itemize}
					\item $r_{BB'} \sim 100\Omega$ 
					\item $c_{B'E} \sim 10pF $
					\item $c_{B'C} \sim 1pF $
				\end{itemize}
			\end{minipage}
		\end{subfigure}
	\end{figure}
\end{center}


A partir de ce schéma on peut calculer la fréquence de transition, paramètre intrinsèque au transistor,
définie comme la fréquence à laquelle le gain en courant du transistor est unitaire.
$f_\tau = f$ , telle que $|\beta| =\left|\left.\frac{i_c}{i_b}\right|_{v_cc=0}\right|=1$ 
On trouve alors:

\[ 
\beta(\omega)= \frac{g_mr_{B'E}}{1+j(C_{B'E}+C_{B'C}r_{B'E}\omega)} \text{ et } f_\tau = \frac{g_m}{2\pi(C_{B'E}+C_{B'C})}
\]

\subsection{Polarisation du transistor}
\paragraph{Inconvénient :} Le point de repos dépend beaucoup de $\beta$. Mais $\beta$ varie d’un transistor à l’autre bien
que la référence soit la même et pour un même transistor en fonction de la température. Ce montage
très simple est donc difficilement utilisable.

Cette contre-réaction assure une stabilisation par rapport à la température : si T augmente, alors le gain
en courant $\beta$  augmente entraînant $I_C$ . Le même raisonnement que précédemment va alors permettre
une stabilisation de $I_C$ malgré l'augmentation de $T$

Dans la structure 2, grâce à $R_E$ , le point de polarisation est donc stable en température et
l’interchangeabilité des transistors est possible.
\subsection{Application d'un signal alternatif (capacités de liaison et de découplage)}

Nous voici en présence d'un amplificateur B.F. (basses fréquences) complet :

Comme le signal d’entrée ajouté est un signal alternatif, les alternances viendront s'ajouter à la tension
de polarisation existante, comme le montre la figure ci-dessous. On se déplace alors sur la droite de
charge dynamique du montage. Cette droite de charge dynamique est déterminée en se plaçant dans
la bande passante du montage et en faisant un schéma dynamique du montage.
\subsubsection{Exemple du montage à charges réparties}

Soit le montage amplificateur à charges réparties suivant :
Dans la bande passante, les capacités de liaison sont des court circuits et pour faire le schéma
dynamiques on éteint toutes les sources non commandées (source de tension éteinte = CC et source de
courant éteinte = CO). Le schéma dynamique dans la bande passante du montage précédent est donc


La droite de charge dynamique passe par le point de polarisation et est donc, dans ce cas, confondue
avec la droite de charge statique. Si l'on veut rester en régime linéaire afin de ne pas déformer le
signal, on voit comme le montre la figure ci-dessous, qu'il faut limiter l'amplitude des signaux : on
reste en petits signaux (=faible amplitude).

Dans le cas contraire, on aura une déformation soit par blocage du transistor, soit par saturation.
Pour notre exemple :
La dynamique par saturation est : $v_{cesat} = V_{ce0}$
La dynamique par blocage est $v_{cebloc} =V_{cc} - V_{ce0}$
La dynamique maximale du montage est donc la plus petite des deux : $v_{cemax}=min(v_{cesat},v_{cebloc}$)
La dynamique de sortie est l'amplitude maximale que peut atteindre le signal de sortie sans
déformation. Dans ce cas du montage à charge répartie, on a :
$v_{smax} =\frac{R_c}{R_c+R_e}v_{cemax}$

Pour assurer une dynamique maximale il faut placer le point de polarisation au milieu de la droite de
charge dynamique, soit pour notre exemple : $V_{ce0} =V_{CC/2}$.
\subsubsection{Exemple du montage émetteur commun}
Soit le montage émetteur commun (=émetteur à la masse dans la bande passante) suivant :

Dans la bande passante, les capacités de liaison et la capacité de découplage C e sont des court-circuits et pour faire le schéma dynamique on éteint toutes les sources non commandées. Le schéma dynamique dans la bande passante du montage précédent est donc :

La droite de charge dynamique (en trait continu sur la figure ci-dessous) passe par le point de polarisation et est donc, dans ce cas, est différente de la droite de charge statique (en pointillée sur la figure ci-dessous). Si l'on veut rester en régime linéaire afin de ne pas déformer le signal, on voit
comme le montre la figure ci-dessous, qu'il faut limiter l'amplitude des signaux : on reste en petits signaux (=faible amplitude).

Dans le cas contraire, on aura une déformation soit par blocage du transistor, soit par saturation. Pour
notre exemple :
La dynamique par saturation est : $v_{cesat} = V_{ce0}$
La dynamique par blocage est $v_{cebloc}$ = $R_C I_{c0}$

La dynamique maximale du montage est donc la plus petite des deux :
$v_{cemax}=min(v_{cesat},v_{cebloc}$

La dynamique de sortie, dans le cas du montage émetteur commun est : $v_{smax} = v_{cemax}$
Pour assurer une dynamique maximale il faut placer le point de polarisation au milieu de la droite de
charge dynamique, soit pour notre exemple :$v_{ce0}$ = $R_C I_{c0}$
\end{document}