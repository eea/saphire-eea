s=tf('s');
gm=22*10^(-3)
Rd=1000
Rs=530
Cs=40*10^(-6)
R1=220*10^(3)
R2=117*10^(3)
R1pR2=R1*R2/(R1+R2)
Re=10*10^(3)
Ci=18*10^(-9)

H=(-gm*(Rd/(1+gm*Rs))*((1+Rs*Cs*s)/(1+((Rs*Cs*s)/(1+gm*Rs))))*(R1pR2*Ci*s/(1+(R1pR2+Re)*Ci*s)))
bode(H)