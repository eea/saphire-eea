\documentclass[../main.tex]{subfiles}

\begin{document}
\subsection{Introduction}
\subsubsection{Hypothèses}
	Point de vue macroscopique:
	\begin{itemize}
		\item les paramètres fixes sont dit \textit{extérieurs}
		\item A l'aide de ces derniers on détermine les \textit{variables internes}.
	\end{itemize}
	On connaît la thermodynamique classique et macroscopique:
	\[\boxed{\d E = \delta W + \delta Q + \sum_{i}^{} \mu_i \d N_i}\]\\

Dans le cadre du cours on considère :
\begin{itemize}
	\item $\delta W = -p \d V$ (gaz)
	\item $\delta Q = T \d S $ (réversible)
\end{itemize}

\subsubsection{Situations canoniques}
Situations type en physique statistique : 
\paragraph{Micro-canonique}
	\begin{itemize}
		\item isolé thermiquement
		\item $E,V$ et $N$ sont fixés .
	\end{itemize}
\paragraph{Canonique :}
	\begin{itemize}
		\item Système en liaison avec un thermostat
		\item L'ensemble $\{\mathrm{Systeme+Thermostat}\}$ est en situation micro-canonique.
	\end{itemize}
\paragraph{Grand canonique :}
	\begin{itemize}
		\item Système canonique
		\item Avec des sources de particules.
	\end{itemize}

\subsection{Postulats $\mu-\mbox{canoniques}$}
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\linewidth]{image/sys_u_canonique}
	\caption{Situation micro-canonique}
	\label{fig:sysucanonique2}
\end{figure}
On note $(l)$ l'ensemble des micro-état du système ( configuration possible vérifiant les paramètres extérieurs). et $\Omega = \Card (l) $

\subsubsection{Premier Postulat}
En situation micro-canonique la probabilité d'être dans un micro-état compatible est uniforme :

\[p_l^* = \begin{syslin}
1/\Omega  \mbox{ si } E_l^* = (E \pm \delta E)\\
0 \mbox{ sinon}
\end{syslin}\]

\subsubsection{Deuxième Postulat}
En situation micro-canonique l'entropie est maximale à l'équilibre thermodynamique.

\subsubsection{Équivalence des deux postulats}
On sait que $S = -k_B \sum p_l ln(p_l)$ et $\sum p_l = 1$.
Être à l'équilibre c'est rendre stationnaire $S$ sous la contrainte.\\

On veux donc maximiser le lagrangien :
$\mathcal{L}(p_l,\lambda) = S(p_l)-\lambda (\sum p_l -1)$

\[\begin{syslin}
\derivp[\mathcal{L}]{\lambda} = 0 = \sum p_l -1 \\
\derivp[\mathcal{L}]{p_l} = 0,~\forall l 
\end{syslin} \]

Or $\derivp[\mathcal{L}]{p_l} = 0  = -k_B(ln(p_l)+1)-\lambda $ 

Et avec $ \forall l , p_l = 1/\Omega$ on a finalement :
\[\boxed{S=k_B\ln(\Omega)}\] 

C'est la valeur maximale de l'entropie de ce système (admis)

\subsection{Dénombrement des états}
\subsubsection{Formules de Stirling}
\begin{eqnarray}
	\boxed{\ln(n!) \simeq n\ln(n)-n}\\
	\boxed{\deriv[\ln(n!)]{n} = \ln(n)}\\
	\boxed{n! \simeq \left(\frac{n}{e}\right)^n\sqrt{2\pi n }}
\end{eqnarray}

\subsubsection{Étude d'un système physique simple en situation $\mu$-canonique}

On reprend le système introduit en début de chapitre : 
\begin{itemize}
	\item $N$ particules discernables
	\item $E = \sum n_i \epsilon_i$ 
	\item $p$ niveau d'énergie $\epsilon_1 \dots \epsilon_p$
\end{itemize}

Le nombre d'états possibles est : 
\begin{equation}
\boxed{\Omega(n_1,...,n_p) = \frac{N!}{\prod_{i=1}^{p} n_i!}}
\end{equation}

On cherche ensuite à maximiser l'entropie sous la contrainte :
\[\mathcal{L}(n_i,\lambda,\mu)= k_B \ln\left(\Omega(n_1\dots n_p)\right)-\lambda\left(E-\sum n_i \epsilon_i\right)-\mu\left( N-\sum n_i\right)\]

\[\begin{syslin}
\displaystyle\derivp[\mathcal{L}]{\lambda} = 0 \implies E = \sum n_i \epsilon_i ~ ~(1) \\
\displaystyle\derivp[\mathcal{L}]{\mu} = 0 \implies N= \sum n_i  ~ ~ ~ ~(2) \\
\end{syslin}
\]

\[\derivp[\mathcal{L}]{n_i} = 0 \implies 0 = -k_B \deriv[]{n_i}\sum_{k=1}^{p}\ln(n_k!) + \lambda \epsilon_i +\mu\]
on a donc :
\begin{align*}
	0 &= -k_B \deriv[]{n_i}\sum_{k=1}^{p}\ln(n_k!) + \lambda \epsilon_i +\mu \\
	0 &= -k_B \ln(n_i)+\lambda \epsilon_i + \mu \\
	n_i &= \frac{\mu}{k_B} \exp(\frac{\lambda\epsilon_i}{k_B})
\end{align*}
Avec $(1)$ on pose : $C =\displaystyle\frac{\mu}{k_B}= \frac{N}{\displaystyle\sum_{k=1}^p \exp\left(\frac{\lambda \epsilon_k}{k_B}\right)}$\\
et on a:
\[n_i =\frac{N}{\displaystyle\sum_{k=1}^p \exp(\frac{\lambda \epsilon_k}{k_B})} \exp(\frac{\lambda\epsilon_i}{k_B})\]

En reprenant la thermodynamique classique macroscopique : 
\begin{align*}
	\d E &= \delta Q +\delta W \\
	\d \left(\sum n_i \epsilon_i \right) &= \underbrace{\sum \d n_i \epsilon_i}_{\delta Q} + \underbrace{\sum n_i \d\epsilon_i}_{\delta W}\\
\end{align*}
Le terme $\sum n_i \d\epsilon_i$ n'est présent que pour $S$ fixé (car $\d S = \delta Q /T $)
\begin{align*}
	\d S =  \frac{\delta Q }{T} =  \frac{1}{T} \sum \d n_i \epsilon_i &= k_B \left(\d\ln(N!) - \sum_{i=1}^p \d \ln(n_i!) \right)\\
	\frac{1}{T} \sum \d n_i \epsilon_i &= -k_B \sum_{i=1}^{p} \ln(n_i) \d n_i
\end{align*}
On note $ A =\frac{N}{\displaystyle\sum_{k=1}^p \exp(\frac{\lambda \epsilon_k}{k_B})}$ et on a :
\begin{align*}
\frac{1}{T} \sum_{i=1}^{p} \d n_i \epsilon_i &= -k_B\left( \sum_{i=1}^{p} \ln(A) +\frac{\lambda \epsilon_i}{k_B}\d n_i\right)\\
\text{Or }~ ~ \sum_{i=1}^{p} \ln(A)\d n_i &= \ln(A) \sum_{i=1}^{p} \d n_i = \ln(A) \d\left(\sum_{i=1}^{p} n_i\right)= 0 \\
\text{D'ou} ~ ~ \frac{1}{T} \sum \d n_i \epsilon_i &= -\lambda \sum \epsilon_i \d n_i \\
\lambda &= -\frac{1}{T}
\end{align*}
On note $\beta = \frac{1}{k_BT}$
On peux donc exprimer les quantités macroscopiques : 
\[ Z_{sp} =  \sum_{i=1}^{p} \exp(-\beta \epsilon_i) \]
\begin{align*}
	E &= \sum n_i \epsilon_i \\
	  &= -\frac{N}{Z_{sp}}\derivp[Z_{sp}]{\beta}\ln(N) 
\end{align*}
Alors on pose  $\boxed{Z= Z_{sp}^N}$
et on a finalement 
\begin{eqnarray}
	\boxed{ E= -\derivp[]{\beta} \ln(Z)}\\
	\boxed{n_i = \frac{N}{Z_{sp}}e^{-\beta\epsilon_i}}\\
	\boxed{p_i = \frac{1}{Z_{sp}}e^{-\beta\epsilon_i}}
\end{eqnarray} 

De plus :
\begin{align*}
	S &= k_B\left[N\left(\ln(N)- \cancel{1}\right) -\sum_{i=1}^{p} n_i \left(\ln(n_i) \cancel{1}\right) \right]\\
	&= k_B N \left[\ln(N) - \sum_{i=1}^{p}n_i\left( \cancel{\ln(N)}-\ln(Z_{sp}-\beta \epsilon_i)\right)\right]\\
	&= k_B \left[ N\ln(Z_{sp})+\beta E\right]\\
	\Aboxed{S &= k_b \ln(Z) +\frac{E}{T}}
\end{align*}

On introduit donc l'énergie libre de \textsc{Helmolz} :
\[\boxed{F=E-TS = k_B T \ln(Z)} \]


\subsection{Postulat canonique}
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\linewidth]{sys_canonique}
	\caption{Situation canonique}
	\label{fig:syscanonique}
\end{figure}

Le thermostat $\mathtt{T}$ impose une température $T^*$. Le système $\{\mathtt{T+S}\}$ est en situation micro-canonique.

On a $E_{\mathtt{S+T}} = E_{tot} = E_{\mathtt{S}} + E_{\mathtt{T}}$ avec $E_\mathtt{T} \gg E_\mathtt{S}$. On désigne par $\centerdot_l$ et $\centerdot_L$ les micro-états respectivement du système $\mathtt{S}$ et $\mathtt{T}$.


\begin{align*}
	\frac{1}{T^*} &= \derivp[S_{\mathtt{T}}]{E}(E_L = E_{tot} - E_l)\\
	&= \derivp[S_{\mathtt{T}}]{E}(E_L = E_{tot}) - \cancelto{0 \mbox{ car }E_\mathtt{T} \gg E_\mathtt{S}}{\derivpp[S_{\mathtt{T}}]{E}(E_L = E_{tot}) \times E_l}\\
	S_{\mathtt{T}}(E_L = E_{tot} - E_l)&=S_{\mathtt{T}}(E_L = E_{tot})-\derivp[S_{\mathtt{T}}]{E}(E_{tot})\times E_l\\
	\Aboxed{S_{\mathtt{T}}(E_L=E_{tot}-E_l)&=S_{\mathtt{T}}(E_L = E_{tot})-\frac{E_l}{T^*}}
\end{align*}

On a : \[S_{tot}^* = - k_B \sum_{L,l} p_{L,l} \ln p_{L,l}\]

Or : \[p_{L,l} = p_L^*(E_{tot}-E_l)\times p_l^c(E_l)\]
Le système \texttt{S} ne perturbe pas le thermostat \texttt{T}, donc \texttt{T} est en état micro-canonique. 

\begin{align*}
	S_{tot} &= -k_B \sum_{L,l}p_L^* p_l^c\left(\ln p_L^* + \ln p_l^* \right)\\
	&=-k_B \left(\sum_{L}p_L^*\ln p_L^*\sum_{l}p_l^c+\sum_{l}p_l^*\sum_{l}p_l^c\ln p_l^*\right)\\
	&=-k_B \left(\cancel{-\frac{S_{\mathtt{S}}}{k_B}} {\sum_{L}p_L^*\ln p_L^*}~~\cancel{\sum_{l}p_l^c}~~+~~\sum_{l}p_l^*\cancel{-\frac{S_{\mathtt{T}}^*(E_{tot}-E_l)}{k_B}}{\sum_{l}p_l^c\ln p_l^*} \right)\\ 
\end{align*}

\[\mbox{Or } S_{\mathtt{T}}^*(E_{tot}-E_l) = S_{\mathtt{T}}^*(E_{tot}) - \frac{E_l}{T^*}\]

\begin{align*}
	S_{tot} &= S_{\mathtt{S}} + S_{\mathtt{T}}^*(E_{tot}) - \frac{\sum_{l}p_l^c E_l}{T^*}\\
	&= S_{\mathtt{S}} + S_{\mathtt{T}}^*(E_{tot}) - \frac{E_{\mathtt{S}}}{T^*}\\
	&= S_{\mathtt{S}} - \frac{1}{T^*}\left(E_{\mathtt{S}} - T^*S_{\mathtt{T}}^*(E_{tot})\right)\\
	\Aboxed{S_{tot} &= S_{\mathtt{S}} - \frac{1}{T^*} F_{\mathtt{S}}}
\end{align*}

Donc le système total étant en situation micro-canonique, l'état d'équilibre statistique est celui qui minimise l'énergie libre du système \texttt{S} à l'équilibre.

\end{document}