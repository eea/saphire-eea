#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  9 11:05:17 2018

@author: pierre-antoine
"""

import numpy as np
import matplotlib.pyplot as plt


class Tf():
    
    p = complex(0,1)
    def __init__(self,num, den,r=False) :
        self.num = np.poly1d(num, r, variable="p")
        self.den = np.poly1d(den, r, variable ="p")

    def __repr__(self):
        return str(self.num) + "\n"+ "-"*len(str(self.den))+"\n"+str(self.den)
            
    
    def is_stable(self):
        if (self.den.c > 0).all or (self.den.c < 0).all:
            print("les coefficient du dénominateur sont du même signe ")
            ln1=[ c for c in self.den.c[::2]]
            ln2=[ c for c in self.den.c[1::2]]
            d= len(ln1)-len(ln2)
            if d > 0 :
                ln2.append(*([0]*d))
            else:
                ln1.append(*([0]*abs(d)))
                
            rooth=[ln1,ln2]
            for i in range (self.den.order):
                l=[]
                for k in range(1,len(rooth[-1])):
                    l.append((rooth[-1][0]*rooth[-2][k]-rooth[-2][0]-rooth[-1][k])/rooth[-1][0])
                l.append(*([0]*abs(len(rooth[0])-len(l))))
                rooth.append(l)  
            return rooth
        else:
            return "den de signe non constant, instable."
        
        
    def bfu(self):
        return Tf(self.num.c, (self.num+self.den).c)
         
        
        
    def bode(self):
        n0=-2                           # Début en x=10**n0
        n1=2                            # Fin   en x=10**n1
        n=500                         # Nombre de pas
        x = np.logspace(n0, n1, num=n)  # Définition x
        
        def GdB(x):
          return 20*np.log10(np.abs(self.num(x)/self.den(x)))
        def phi(x):
          return 180/np.pi*(np.angle(self.num(x))-np.angle(self.den(x)))
        fig=plt.figure()
        # Titre général
        fig.suptitle("Diagramme de Bode", size='x-large')
        # Création de deux sous figures qui partage le même axe x : GdB et phase 
        # GdB placé en haut (ax1) est 2 fois plus haut que la phase (ax2)
        ax1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
        ax2 = plt.subplot2grid((3,1), (2,0), sharex=ax1)
        
        # Axes du gain
        ax1.semilogx(x,GdB(x),linewidth=1.5)
        
        ymin,ymax=np.ceil(min(GdB(x))),np.ceil(max(GdB(x)))
        yticks = np.arange(ymin,ymax+1,20)                  # pour compter de 20 en 20 sur l'axe de GdB
        ax1.yaxis.set_ticks(yticks)                         # et l'écrire
        ax1.axis([10**n0,10**n1,ymin,ymax])                 # Axes de la courbe
        ax1.set_ylabel(r'$G_\mathrm{dB}$')    # Titre de l'axe vertical
        ax1.grid(True, which='major',color='black', linestyle='-',linewidth=1)
        ax1.grid(True, which='minor',color='black', linestyle='-',linewidth=0.5)
        plt.setp(ax1.get_xticklabels(), visible=False)
       #PHASe
        ax2.semilogx(x,phi(x),linewidth=1.5)
        
        ymin=-180
        ymax=180
        yticks = np.arange(ymin,ymax+1,60)
        ax2.yaxis.set_ticks(yticks)                   # Axes de la courbe
        ax2.set_ylabel(r'$\varphi\, (^\circ)$')        # Titre de l'axe vertical
        ax2.set_xlabel(r'$x$ (échelle log)')      # Titre de l'axe horizontal
        ax2.grid(True, which='major',color='black', linestyle='-',linewidth=1)
        ax2.grid(True, which='minor',color='black', linestyle='-',linewidth=0.5)
        
        plt.subplots_adjust(hspace = 0.1) # espacement entre les graphes

        fig.show()

H = Tf([10],[0.1,0.01,0.05,0.003,5])
print(H)
H.bode()
print(H.is_stable())
print(H.bfu())


